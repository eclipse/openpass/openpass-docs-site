..
  *******************************************************************************
  Copyright (c) 2021 in-tech GmbH
                2023 Mercedes-Benz Tech Innovation GmbH

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

openPASS Documentation
======================

The openPASS (Open Platform for Assessment of Safety Systems) tool is a developed framework for the simulation of interaction between traffic participants to evaluate and parametrize active safety systems. 
The simulation is based on a specific situation configuration and can contain several simulation runs, which differ due to random parameters.

The software suite of openPASS started as a set of stand-alone applications, which can be installed and configured individually.
In the future, the graphical user interface opGUI will evolve to a single entry point, enabling the average user to use openPASS as a "monolithic" tool.

This guide contains information about installation, configuration and usage of all tools in the |Op| environment.

.. toctree::
   :caption: Installation Guide
   :glob:
   :maxdepth: 1

   installation_guide/*

.. toctree::
   :caption: User Guides
   :glob:
   :maxdepth: 1

   user_guide/*

.. toctree::
   :caption: Advanced topics
   :glob:
   :maxdepth: 1

   advanced_topics/*

.. toctree-api::
   :caption: Developer Information
   :glob:
   :maxdepth: 1

   developer_information/*
   :api-doc:

.. toctree::
   :caption: Other Information
   :glob:
   :maxdepth: 1

   other_information/*

Todolist
========

.. todolist::
