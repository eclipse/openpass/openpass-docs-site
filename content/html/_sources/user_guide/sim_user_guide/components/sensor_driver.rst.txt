..
  *******************************************************************************
  Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)

  This program and the accompanying materials are made available under the
  terms of the Eclipse Public License 2.0 which is available at
  http://www.eclipse.org/legal/epl-2.0.

  SPDX-License-Identifier: EPL-2.0
  *******************************************************************************

.. _sensor_driver:

Sensor_Driver
#############

This module collects and distributes data from the WorldInterface and AgentInterface concerning the own vehicle, traffic rule 
and geometry information and surrounding object. It creates a data struct to store the world and agent data of the own vehicle, 
traffic rule and geometry information, other objects and surrounding vehicles.

Components of the module
========================

The information collected by SensorDriver consists of generally four different data structs:

* :ref:`sensor_driver_own_vehicle_states_` includes all information about the vehicle of the driver (e.g. speed, acceleration, ...)
* :ref:`sensor_driver_traffic_rule_information_` includes information about the static environment (e.g. traffic signs, lane markings, ...)
* :ref:`sensor_driver_geometry_information_` includes information about surrounding lanes that describe its geometric features
* :ref:`sensor_driver_surrounding_objects_` includes information about objects surrounding the own agent as seen by the driver

.. _sensor_driver_own_vehicle_states_:

OwnVehicleStates
----------------

The Information of the own vehicle are based on the following general **struct OwnVehicleInformation** :

.. table::
   :class: tight-table

   ====================================== ======= ===== ================================================================================== =============
   Variable                               Type    Unit  Description                                                                        Default value
   ====================================== ======= ===== ================================================================================== =============
   absoluteVelocity                       Double  m/s   Velocity of agent                                                                  -999
   acceleration                           Double  m/s²  Acceleration of agent                                                              -999
   lateralPosition                        Double  m     t-coordinate                                                                       -999
   heading                                Double  rad   Heading relative to lane                                                           -999
   steeringWheelAngle                     Double  rad   Angle of the steering wheel                                                        -999
   distanceToLaneBoundaryLeft             Double  m     Distance between the left front point and the left boundary of the lane it is in   -999
   distanceToLaneBoundaryRight            Double  m     Distance between the right front point and the right boundary of the lane it is in -999
   collision                              Boolean       Whether this agent has collided with another object                                false
   ====================================== ======= ===== ================================================================================== =============

.. _sensor_driver_traffic_rule_information_:

TrafficRuleInformation
----------------------

The struct TrafficRuleInformation contains infrastructure information of surrounding lanes that are used to establish traffic rules.

The general structure is shown in the following table.

.. table::
   :class: tight-table

   ============================== =====================================================================================
   Variable                       Description                                                                        
   ============================== =====================================================================================
   laneLeft                       Data about the lane to left (in driving direction) of the mainLane                       
   laneEgo                        Data about the lane the where the middle of the front of the agent is (i.e. mainLane)
   laneRight                      Data about the lane to right (in driving direction) of the mainLane
   laneMarkingsLeft               List of lane markings on the left side of mainLane
   laneMarkingsRight              List of lane markings on the right side of mainLane
   laneMarkingsLeftOfLeftLane     List of lane markings on the left side of lane to the left
   laneMarkingsRightOfRightLane   List of lane markings on the right side of lane to the right
   ============================== =====================================================================================

The basis for the respective variables is the following general struct **LaneInformationTrafficRules**, which is used to trasport data of a lane concerning traffic rules as seen by the driver.

.. table::
   :class: tight-table

   ============== ===============================================================================
   Variable       Description                                                                        
   ============== ===============================================================================
   trafficSigns   Vector of all traffic signs valid for this lane inside the visibility distance                    
   trafficLights  Vector of all traffic lights valid for this lane inside the visibility distance
   ============== ===============================================================================

.. _sensor_driver_geometry_information_:

GeometryInformation
-------------------

The struct GeometryInformation contains infrastructure information of surrounding lanes that describe its geometric features.

The general structure is shown in the following table.

.. table::
   :class: tight-table

   =================== =====================================================================================
   Variable            Description                                                                        
   =================== =====================================================================================
   visibilityDistance  Current maximum visibility distance as specified by the world                      
   laneLeft            Data about the lane to left (in driving direction) of the mainLane
   laneEgo             Data about the lane the where the middle of the front of the agent is (i.e. mainLane)
   laneRight           Data about the lane to right (in driving direction) of the mainLane
   =================== =====================================================================================

The basis for the respective variables is the following general struct **LaneInformationGeometry**, which is used to transport data of a lane concerning its geometric features as seen by the driver.

.. table::
   :class: tight-table

   =================== ======= ===== ================================================================= =============
   Variable            Type    Unit  Description                                                       Default value
   =================== ======= ===== ================================================================= =============
   exists              Boolean       Whether there is a lane on this position                          false
   curvature           Double  1/m   Curvature at current s position (default if not existing)         -999 
   width               Double  m     Width at current s position (default if not existing)             -999   
   distanceToEndOfLane Double  m     Distance from current position to the end of the lane or          -999  
                                     infinity if the end is farther away than the visibility distance      
   =================== ======= ===== ================================================================= =============

.. _sensor_driver_surrounding_objects_:

SurroundingObjects
------------------

This struct SurroundingObjects contains data of the objects surrounding the own agent as seen by the driver. For all cases mainLaneId is used as own lane.

.. table::
   :class: tight-table

   ================= ========================================================
   Variable          Description                                             
   ================= ========================================================
   objectFront       Next object in the same lane
   objectRear        Closest object behind the agent in the same lane
   objectFrontLeft   Next object in the lane to the left
   objectRearLeft    Closest object behind the agent in the lane to the left
   objectFrontRight  Next object in the lane to the right
   objectRearRight   Closest object behind the agent in the lane to the right
   ================= ========================================================

The basis for the respective variables is the following general struct **ObjectInformation**, which is used to transport data of an object as seen by the driver:

.. table::
   :class: tight-table

   ====================================== ======= ===== ============================================================================================== =============================
   Variable                               Type    Unit  Description                                                                                    Default value
   ====================================== ======= ===== ============================================================================================== =============================
   id                                     Integer       Id of the object                                                                               -1 (not existing)
   exist                                  Boolean       False if there is no object in this position                                                   false
   isStatic                               Boolean       True if stationary object, false if agent                                                      false
   absoluteVelocity                       Double  m/s   Absolute velocity of the agent                                                                 -999 (object is not an agent)
   acceleration                           Double  m/s²  Acceleration of the agent                                                                      -999 (object is not an agent)
   heading                                Double  rad   Heading relative to the street                                                                 -999 (not existing)
   length                                 Double  m     Length of object                                                                               -999 (not existing)
   width                                  Double  m     Width of object                                                                                -999 (not existing)                           
   height                                 Double  m     Heigth of object                                                                               -999 (not existing)                      
   relativeLongitudinalDistance           Double  m     Relative distance along the road (i.e. in direction s) between own agent and object            -999 (not existing)                     
   relativeLateralDistance                Double  m     Relative distance at right angle to the road (i.e in direction t) between own agent and object -999 (not existing)           
   ====================================== ======= ===== ============================================================================================== =============================
